import junit.framework.TestCase;
import org.junit.Test;
public class ComplexTest extends TestCase {
    Complex p1=new Complex(1,1);
    Complex p2=new Complex(1,-1);
    Complex p3=new Complex(2,1);
    Complex p4=new Complex(3,-1);
    @Test
    public void testAdd(){
        assertEquals("2.0+0.0i",p1.ComplexAdd(p2).toString());
        System.out.println(p1.ComplexAdd(p2));
    }
    @Test
    public void testSub(){
        assertEquals("0.0+2.0i",p1.ComplexSub(p2).toString());
        System.out.println(p1.ComplexSub(p2));
    }
    @Test
    public void testMut(){
        assertEquals("2.0+0.0i",p1.ComplexMulti(p2).toString());
        System.out.println(p1.ComplexSub(p2));
    }
    @Test
    public void testDiv(){
        assertEquals("0.0+1.0i",p1.ComplexDiv(p2).toString());
        System.out.println(p1.ComplexDiv(p2));
    }
    @Test
    public void testAdd_2(){
        assertEquals("5.0+0.0i",p3.ComplexAdd(p4).toString());
        System.out.println(p3.ComplexAdd(p4));
    }
    @Test
    public void testSub_2(){
        assertEquals("-1.0+2.0i",p3.ComplexSub(p4).toString());
        System.out.println(p3.ComplexSub(p4));
    }
    @Test
    public void testMut_2(){
        assertEquals("7.0+1.0i",p3.ComplexMulti(p4).toString());
        System.out.println(p3.ComplexMulti(p4));
    }
    @Test
    public void testDiv_2(){
        assertEquals("0.5+0.5i",p3.ComplexDiv(p4).toString());
        System.out.println(p3.ComplexDiv(p4));
    }
}
